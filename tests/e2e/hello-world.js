const {Builder, By, Key, until} = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const test = require('colored-tape');

test('Hello world is displayed', async (t) => {

  const chromeOptions = new chrome.Options();
  chromeOptions.addArguments('--headless', '--disable-gpu', '--no-sandbox', '--disable-extensions', '--disable-dev-shm-usage');

  let driver = await new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();

  // Values
  const url = 'http://localhost:3000/';

  console.log('Testing ' + url);

  try {
    await driver.get(url);
    console.log('Opened ' + url);

    const text = await driver.findElement(By.id('hello-world')).getText();

    t.equal(text, 'Hello World!', 'Response should be Hello World!');

  } finally {
    await driver.quit();
  }

  t.end();
});
